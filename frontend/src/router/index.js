import Vue from 'vue'
import Router from 'vue-router'
import Messages from '@/components/Messages'
import Login from '@/components/Login'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/messages',
      name: 'messages',
      component: Messages
    }
  ]
})
