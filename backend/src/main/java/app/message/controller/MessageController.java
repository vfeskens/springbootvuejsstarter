package app.message.controller;

import app.message.domain.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
@RequestMapping(path = "/api")
public class MessageController {

    // Messages currently kept in memory
    private List<Message> messagesInMemory = new ArrayList<>();

    @RequestMapping(value = "/messages", method = RequestMethod.GET)
    public List<Message> getMessages() {
        log.info("Incoming GET /messages call.");
        return messagesInMemory;
    }

    @RequestMapping(value = "/message", method = RequestMethod.POST)
    public void postMessage(@RequestBody final Message message) {
        log.info("Incoming POST /message call. Message : {}", message);
        messagesInMemory.add(message);
    }
}
