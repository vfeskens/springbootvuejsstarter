package app.message.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Message {
    private String from;
    private String content;

    private Message(String from, String content) {
        this.from = from;
        this.content = content;
    }

    public static Message of(final String from, final String content) {
        return new Message(from, content);
    }
}
